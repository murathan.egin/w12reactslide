export const SliderData = [
    {
        image: 'https://images.unsplash.com/photo-1607253471070-f0aa5d6487f8?ixid=MXwxMjA3fDB8MHxwcm9maWxlLXBhZ2V8M3x8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
    },
    {
        image: 'https://images.unsplash.com/photo-1605792260642-7af0ec5f69fc?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8dHJhYnpvbnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=60'
    },
    {
        image: 'https://images.unsplash.com/photo-1606599265085-ee132d0e41c5?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NDB8fHRyYWJ6b258ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
    },
    {
        image: 'https://images.unsplash.com/photo-1526048598645-62b31f82b8f5?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8dHVya2V5fGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
    }
];